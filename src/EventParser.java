public class EventParser {

    public void parseStr(String s, CarMonitor mon) {
        switch (s) {
            case "gas":
                mon.fullGas();
                break;
            case "brake":
                mon.fullBrake();
                break;
        }
        if (s.startsWith("speed limit ")) {
            int speedLimit = Integer.parseInt(s.substring(12));
            mon.newSpeedLimit(speedLimit);
        }
    }
}
