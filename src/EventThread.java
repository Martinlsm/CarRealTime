import java.util.Scanner;

public class EventThread extends Thread {

    private CarMonitor mon;
    private Scanner scan;
    private EventParser parser;

    public EventThread(CarMonitor mon) {
        this.mon = mon;
        scan = new Scanner(System.in);
        parser = new EventParser();
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            String line = scan.nextLine();
            System.out.println("line = " + line);
            parser.parseStr(line, mon);
        }
    }

}
