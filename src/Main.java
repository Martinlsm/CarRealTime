public class Main {

    public static void main(String[] args) {
        CarMonitor mon = new CarMonitor();
        TimeThread timeThread = new TimeThread(mon);
        ConsoleThread consoleThread = new ConsoleThread(mon);
        EventThread eventThread = new EventThread(mon);
        timeThread.start();
        consoleThread.start();
        eventThread.start();
    }
}
