public class ConsoleThread extends Thread {

    private static final long UPDATE_INTERVAL_MILLIS = 3000;

    private CarMonitor mon;

    public ConsoleThread(CarMonitor mon) {
        this.mon = mon;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                Thread.sleep(UPDATE_INTERVAL_MILLIS);
            } catch (InterruptedException e) { }
            mon.printState();
        }
    }
}

