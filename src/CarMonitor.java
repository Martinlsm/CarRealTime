public class CarMonitor {

    private static final float MAX_SPEED = (180 / 3.6f);

    private float dist, speed, acceleration;
    private float speedLimit;

    public CarMonitor() {
        dist = 0;               //[m]
        speed = 0;              //[m/s]
        acceleration = 0;       //[m/s²]
        speedLimit = MAX_SPEED;
    }

    public synchronized void update(float dt) {
        speed = speed + acceleration * dt;
        if (speed < 0) {
            speed = 0;
            acceleration = 0;
        } else if (speed > speedLimit && speed < speedLimit + 3) {
            acceleration = 0;
            speed = speedLimit;
        } else if (speed > speedLimit) {
            acceleration = -(5 / 3.6f);
        }
        dist = dist + speed * dt;
    }

    public synchronized void newSpeedLimit(float speedLimit) {
        System.out.println("new speed limit " + speedLimit);
        speedLimit = speedLimit / 3.6f;
        this.speedLimit = speedLimit;
    }

    public synchronized void fullBrake() {
        acceleration = -(15 / 3.6f);
    }

    public synchronized void fullGas() {
        acceleration = 10 / 3.6f;
    }

    public synchronized void printState() {
        System.out.println("Total distance: " + (dist / 1000) + " km");
        System.out.println("Speed: " + (speed * 3.6) + " km/h\n");
    }
}
