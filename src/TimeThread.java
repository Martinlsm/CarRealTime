public class TimeThread extends Thread {

    private static final int UPDATE_INTERVAL_MILLIS = 200;

    private CarMonitor mon;
    private long lastTimeStamp;

    public TimeThread(CarMonitor mon) {
        this.mon = mon;
    }

    @Override
    public void run() {
        lastTimeStamp = System.currentTimeMillis();
        while (!isInterrupted()) {
            try {
                Thread.sleep(UPDATE_INTERVAL_MILLIS);
            } catch (InterruptedException e) { }
            long currentTimeStamp = System.currentTimeMillis();
            long dt = currentTimeStamp - lastTimeStamp;
            lastTimeStamp = currentTimeStamp;
            mon.update((float) dt / 1000);
        }
    }
}
